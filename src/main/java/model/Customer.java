package model;

/**
 * Created by ruaoneill on 15/03/2017.
 */
public class Customer {

    //Example customer information
    private int partyId;
    private String firstName;
    private String lastName;
    private String nationalInsuranceNumber;

    //Example customer pension details
    private double contributionAmount;
    private double percentageOfSalary;

    //Getters and Setters
    public int getPartyId() {
        return partyId;
    }

    public void setPartyId(int partyId) {
        this.partyId = partyId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getNationalInsuranceNumber() {
        return nationalInsuranceNumber;
    }

    public void setNationalInsuranceNumber(String nationalInsuranceNumber) {
        this.nationalInsuranceNumber = nationalInsuranceNumber;
    }

    public double getContributionAmount() {
        return contributionAmount;
    }

    public void setContributionAmount(double contributionAmount) {
        this.contributionAmount = contributionAmount;
    }

    public double getPercentageOfSalary() {
        return percentageOfSalary;
    }

    public void setPercentageOfSalary(double percentageOfSalary){
        this.percentageOfSalary = percentageOfSalary;
    }

}
