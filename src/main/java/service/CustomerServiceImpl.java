package service;

import model.Customer;
import repository.CustomerRepositoryImpl;

import java.util.List;

/**
 * Created by ruaoneill on 15/03/2017.
 */
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepositoryImpl customerRepository = new CustomerRepositoryImpl();

    @Override
    public List<Customer> findAllCustomers(){
        return customerRepository.findAllCustomers();
    }

    @Override
    public Customer returnCustomerPensionDetails(int partyId) {
        return customerRepository.returnCustomerPensionDetails(partyId);
    }
}
