package repository;

import model.Customer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ruaoneill on 15/03/2017.
 */
public class CustomerRepositoryImpl implements CustomerRepository {

    @Override
    public List<Customer> findAllCustomers() {

        List<Customer> customers = new ArrayList<>();

        //Dummy customer 1
        Customer customer1 = new Customer();
        customer1.setPartyId(1234);
        customer1.setFirstName("Brandon");
        customer1.setLastName("Sanderson");
        customer1.setNationalInsuranceNumber("PB123456A");
        customer1.setContributionAmount(800.00);
        customer1.setPercentageOfSalary(20.5);

        //Dummy customer 2
        Customer customer2 = new Customer();
        customer2.setPartyId(5678);
        customer2.setFirstName("Robert");
        customer2.setLastName("Jordan");
        customer2.setNationalInsuranceNumber("PB34567B");
        customer2.setContributionAmount(725.50);
        customer2.setPercentageOfSalary(18.25);

        customers.add(customer1);
        customers.add(customer2);

        return customers;

    }

    @Override
    public Customer returnCustomerPensionDetails(int partyId) {

        //Dummy customer 1
        Customer customer1 = new Customer();
        customer1.setPartyId(1234);
        customer1.setFirstName("Brandon");
        customer1.setLastName("Sanderson");
        customer1.setNationalInsuranceNumber("PB123456A");
        customer1.setContributionAmount(800.00);
        customer1.setPercentageOfSalary(20.5);

        //Dummy customer 2
        Customer customer2 = new Customer();
        customer2.setPartyId(5678);
        customer2.setFirstName("Robert");
        customer2.setLastName("Jordan");
        customer2.setNationalInsuranceNumber("PB34567B");
        customer2.setContributionAmount(725.50);
        customer2.setPercentageOfSalary(18.25);

        if (partyId == customer1.getPartyId()) {
            return customer1;
        }
        else if (partyId == customer2.getPartyId()) {
            return customer2;
        }
        else {
            return null;
        }

    }
}
