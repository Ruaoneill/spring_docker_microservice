package application;

import model.Customer;
import service.CustomerService;
import service.CustomerServiceImpl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by ruaoneill on 15/03/2017.
 */

@SpringBootApplication
@RestController
public class Application {

    CustomerService service = new CustomerServiceImpl();

    @RequestMapping("/")
    public List<Customer> home() {
        System.out.println("A list of all customers: ");
        return (service.findAllCustomers());
    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}

