package service;

import model.Customer;

import java.util.List;

/**
 * Created by ruaoneill on 15/03/2017.
 */
public interface CustomerService {
    List<Customer> findAllCustomers();
    Customer returnCustomerPensionDetails(int partyId);
}
